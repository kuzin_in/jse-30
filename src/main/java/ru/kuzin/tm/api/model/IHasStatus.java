package ru.kuzin.tm.api.model;

import org.jetbrains.annotations.NotNull;
import ru.kuzin.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
